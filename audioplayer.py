import sys
import os
import configparser
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

config = configparser.ConfigParser()
config.read(resource_path('config.ini'))

def CheckKey():
    global config
    if 'API' in config:
        if not 'key' in config['API']:
            return False
        else:
            return True
    else:
        return False

if not CheckKey():
    config.add_section('API')
    config.set('API', 'key', 'test')

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.browser = QWebEngineView()
        self.browser.page().profile().clearHttpCache()
        self.browser.setUrl(QUrl('https://audioplayer.dogit-studios.ru/?audio='+config["API"]["key"]))
        self.setCentralWidget(self.browser)
        self.showMaximized()

app = QApplication(sys.argv)
QApplication.setApplicationName('DogitPlayer')
QApplication.setApplicationDisplayName('DogitPlayer')
QApplication.setWindowIcon(QIcon(resource_path('sound-bars.ico')))
window = MainWindow()
app.exec_()